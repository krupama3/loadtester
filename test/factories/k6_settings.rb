FactoryBot.define do

  factory :k6_settings do
    singleton_guard { 0 }

    trait :external_runner do
      k6_external_runner { true }
      k6_scripts_external_runner_url { 'https://external_runner.org'}
      k6_scripts_external_runner_access_key { 'runner_access_key' }
      k6_scripts_external_runner_testing_client_id { 'client_id' }
    end
  end

end
