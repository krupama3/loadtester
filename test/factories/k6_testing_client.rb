FactoryBot.define do

  factory :k6_testing_client do
    sequence(:name) { |n| "name_#{n}" }
    sequence(:web_url) { |n| "https://www.example#{n}.com" }
    script_access_key { 'script_access_key' }
  end

end
