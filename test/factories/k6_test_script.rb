FactoryBot.define do

  factory :k6_test_script do
    sequence(:title) { |n| "script_#{n}" }
    association :k6_test_scenario
    association :k6_test_type
  end

end
