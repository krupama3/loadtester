FactoryBot.define do

  factory :k6_test_scenario do
    sequence(:title) { |n| "login_#{n}" }
    k6_script_configuration {
      {
        "scenario": {
          "login": {
            "url": "/login",
            "method": "get",
            "params": {
              "tags": {
                "name": "login"
              }
            },
            "form": {
              "form_selector": "#login-form > form",
              "fields": {
                "username": "admin",
                "password": "diplomka"
              },
              "checks": [
                                 {
                                   "message": "logged in successfully",
                                   "subject": "r.status",
                                   "operator": "===",
                                   "compare": "200"
                                 }
                               ]
            },
            "checks": [
                     {
                       "message": "page loaded successfully",
                       "subject": "r.status",
                       "operator": "===",
                       "compare": "200"
                     }
                   ]
          }
        }
      }.to_json
    }
  end

end
