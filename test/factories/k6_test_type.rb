FactoryBot.define do

  factory :k6_test_type do
    sequence(:title) { |n| "type_#{n}" }
    k6_script_configuration {
      {
        "vus": 1,
        "duration": "1s",
        "thresholds": {
          "http_req_failed": ["rate<0.01"],
          "http_req_duration": ["p(99) < 1000"]
        }
      }.to_json
    }
  end

end
