require_relative '../../rails_helper'

RSpec.describe(LoadTesterMetricTypes::Gauge, type: :service) do

  let(:raw_data_file_path) { File.join(Redmine::Plugin.find(:load_tester).directory, "test/fixtures/gauge_raw_data.json").to_s }
  let(:metric_data) { JSON.parse(File.read(raw_data_file_path)) }

  subject(:instance) { described_class.new(metric_data) }

  describe '#process_raw_data' do
    subject { instance.process_raw_data }

    it 'process and modifies input data' do
      subject
      expect(metric_data["raw_data"]).to be_nil
      expect(metric_data["data"].present?).to be_truthy
      expect(metric_data["data"]["last_value"].present?).to be_truthy
      expect(metric_data["data"]["max"].present?).to be_truthy
      expect(metric_data["data"]["min"].present?).to be_truthy
      expect(metric_data["data"]["max"]).to eq(2)
    end
  end

end
