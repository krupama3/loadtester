require_relative '../../rails_helper'

RSpec.describe(LoadTesterMetricTypes::Counter, type: :service) do

  let(:raw_data_file_path) { File.join(Redmine::Plugin.find(:load_tester).directory, "test/fixtures/counter_raw_data.json").to_s }
  let(:metric_data) { JSON.parse(File.read(raw_data_file_path)) }

  subject(:instance) { described_class.new(metric_data) }

  describe '#process_raw_data' do
    subject { instance.process_raw_data }

    it 'process and modifies input data' do
      subject
      expect(metric_data["raw_data"]).to be_nil
      expect(metric_data["data"].present?).to be_truthy
      expect(metric_data["data"]["sum"].present?).to be_truthy
      expect(metric_data["data"]["count"].present?).to be_truthy
      expect(metric_data["data"]["sum"]).to eq(450)
    end
  end

end
