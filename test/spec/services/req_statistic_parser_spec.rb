require_relative '../rails_helper'

RSpec.describe(ReqStatisticsParser, type: :service) do

  let(:request_statistics_raw_data_file_path) { File.join(Redmine::Plugin.find(:load_tester).directory, "test/fixtures/req_statistics.json").to_s }
  let(:thresholds) { ["p(99) < 1000"] }
  let(:request_statistics_raw_data) { JSON.parse(File.read(request_statistics_raw_data_file_path)) }

  subject(:instance) { described_class.new(request_statistics_raw_data, thresholds) }

  describe '#parse' do
    subject { instance.parse }

    it 'parses statistics and return result' do
      statistic_result = subject
      expect(statistic_result.present?).to be_truthy
      expect(statistic_result.values.first.present?).to be_truthy
      expect(statistic_result.values.first.values.first.present?).to be_truthy
      expect(statistic_result.values.first.values.first["count"]).to eq(20)
    end
  end

  describe '#keys' do
    subject { instance.keys }
    it 'returns columns of statistics' do
      keys = subject
      expect(keys).to include('avg', 'p(90)', 'p(99)')
    end

  end

end
