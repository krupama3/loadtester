require_relative '../rails_helper'

RSpec.describe(ResultJsonParser, type: :service) do

  let(:result_file_path) { File.join(Redmine::Plugin.find(:load_tester).directory, "test/fixtures/json_result").to_s }

  subject(:instance) { described_class.new(result_file_path) }

  describe '#parse' do
    subject { instance.parse }

    it 'parses result and creates parsed file' do
      file_path = subject
      expect(File.exist?(file_path)).to be_truthy
      json_content = JSON.parse(File.read(file_path))
      expect(json_content['checks']).not_to be_empty
      File.delete(file_path) if File.exist?(file_path)
    end
  end

end
