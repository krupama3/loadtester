require_relative '../rails_helper'

RSpec.describe(K6TestingClient, type: :model) do

  let(:k6_testing_client) { FactoryBot.create(:k6_testing_client) }

  describe '#create' do
    it 'sets internal name' do
      params = { name: 'Example Testing Client', web_url: 'https://www.example.com', script_access_key: 'sak'}
      client = described_class.create(params)
      expect(client.internal_name).to eq(params[:name].parameterize.underscore)
    end
  end

  describe '#update' do
    it 'creates new instance' do
      k6_testing_client # touch to initialize
      expect do
        k6_testing_client.update(name: 'Changed name')
        k6_testing_client.reload
      end.to change(k6_testing_client, :internal_name)
    end
  end

end
