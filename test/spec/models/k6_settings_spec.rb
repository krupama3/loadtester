require_relative '../rails_helper'

RSpec.describe(K6Settings, type: :model) do

  let(:k6_settings_instance) { FactoryBot.create(:k6_settings) }

  describe '#initialize' do
    context 'instance exists' do
      before(:each) do
        k6_settings_instance # touch to initialize
      end

      it 'wont create new instance' do
        expect{described_class.new}.to change(::K6Settings, :count).by(0)
      end
    end
  end

  describe '#instance' do
    subject { described_class.instance }
    context 'instance exists' do
      before(:each) do
        k6_settings_instance # touch to initialize
      end

      it 'wont create new instance' do
        expect{subject}.to change(::K6Settings, :count).by(0)
      end
    end

    context 'instance does not exist' do
      it 'creates new instance' do
        expect{subject}.to change(::K6Settings, :count).by(1)
      end
    end
  end

end
