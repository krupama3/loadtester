require_relative '../rails_helper'

RSpec.describe(K6ResultsController, type: :controller) do

  let(:example_text_file_path) { File.join(Redmine::Plugin.find(:load_tester).directory, "test/fixtures/#{example_text_file_name}").to_s }
  let(:example_json_file_path) { File.join(Redmine::Plugin.find(:load_tester).directory, "test/fixtures/#{example_json_file_name}").to_s }
  let(:example_text_file_name) { 'example_text_file.txt' }
  let(:example_json_file_name) { 'example_json_file.json' }
  let!(:k6_settings_instance) { FactoryBot.create(:k6_settings, k6_runner_access_key: "access_key", k6_scripts_external_runner_access_key: "access_key") }
  let(:result_file_dir) { Rails.root.join('public', 'k6_run_results') }

  describe 'POST retrieve_k6_results' do

    it 'parses result and creates parsed file' do
      params = {
        access_key: K6Settings.k6_runner_access_key,
        json_file: File.read(example_json_file_path),
        json_file_name: example_json_file_name,
        text_file: File.read(example_text_file_path),
        text_file_name: example_text_file_name
      }

      post :retrieve_k6_results, params: params

      text_file_path = "#{result_file_dir}/#{example_text_file_name}"
      expect(File.exist?(text_file_path)).to be_truthy
      json_file_path = "#{result_file_dir}/#{example_json_file_name}"
      expect(File.exist?(json_file_path)).to be_truthy
      File.delete(text_file_path) if File.exist?(text_file_path)
      File.delete(json_file_path) if File.exist?(json_file_path)
    end
  end

end
