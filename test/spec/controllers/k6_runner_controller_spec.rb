require_relative '../rails_helper'

RSpec.describe(K6RunnerController, type: :controller) do

  let(:k6_test_script) { FactoryBot.create(:k6_test_script) }
  let(:k6_testing_client) { FactoryBot.create(:k6_testing_client) }
  let!(:k6_settings_instance) { FactoryBot.create(:k6_settings, k6_runner_access_key: "access_key") }
  let(:scripts_data) { [{ script_url: k6_test_script_k6_test_script_url(k6_test_script, format: :js), script_name: k6_test_script.title }] }

  describe 'POST run_remote_k6_scripts' do
    before(:each) do
      allow_any_instance_of(K6RemoteRunnerJob).to receive(:perform).with(anything(), k6_testing_client).and_return(true)
    end
    context 'with all credentials' do
      it 'calls K6RemoteRunnerJob' do
        ActiveJob::Base.queue_adapter = :test
        ActiveJob::Base.queue_adapter.perform_enqueued_jobs = true
        params = {
          scripts_data: scripts_data,
          access_key: K6Settings.k6_runner_access_key,
          testing_client_id: k6_testing_client.internal_name,
          format: 'json'
        }
        expect {
          post :run_remote_k6_scripts, params: params
        }.to perform_job(K6RemoteRunnerJob)
      end
    end

    context 'without client_id' do
      it 'returns unprocessable entity error' do
        ActiveJob::Base.queue_adapter = :test
        ActiveJob::Base.queue_adapter.perform_enqueued_jobs = true
        params = {
          scripts_data: scripts_data,
          access_key: K6Settings.k6_runner_access_key,
          testing_client_id: "unknown_client",
          format: 'json'
        }
        expect {
          post :run_remote_k6_scripts, params: params
          expect(response.status).to eq(422)
        }.not_to perform_job(K6RemoteRunnerJob)
      end
    end

    context 'without access_key' do
      it 'returns unprocessable entity error' do
        ActiveJob::Base.queue_adapter = :test
        ActiveJob::Base.queue_adapter.perform_enqueued_jobs = true
        params = {
          scripts_data: scripts_data,
          access_key: "unknown_access_key",
          testing_client_id: k6_testing_client.internal_name,
          format: 'json'
        }
        expect {
          post :run_remote_k6_scripts, params: params
          expect(response.status).to eq(422)
        }.not_to perform_job(K6RemoteRunnerJob)
      end
    end


  end

end
