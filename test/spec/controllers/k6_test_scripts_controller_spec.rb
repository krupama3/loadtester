require_relative '../rails_helper'

RSpec.describe(K6TestScriptsController, type: :controller) do

  let(:k6_test_script) { FactoryBot.create(:k6_test_script) }

  describe 'GET run_k6_script' do
    context 'local runner' do
      before(:each) do
        allow_any_instance_of(K6LocalRunnerJob).to receive(:perform).with(anything()).and_return(true)
      end
      it 'calls K6LocalRunnerJob' do
        ActiveJob::Base.queue_adapter                       = :test
        ActiveJob::Base.queue_adapter.perform_enqueued_jobs = true
        expect {
          get :run_k6_script, params: { id: k6_test_script.id }, xhr: true
        }.to perform_job(K6LocalRunnerJob)
      end
    end

    context 'remote runner' do
      let!(:k6_settings_instance) { FactoryBot.create(:k6_settings, :external_runner) }
      let(:runner_endpoint_url) { "#{K6Settings.k6_scripts_external_runner_url}#{k6_runner_run_remote_k6_scripts_path(format: :json)}" }
      let(:runner_endpoint_params) do
        {
          body: data.to_json,
          headers: { 'Content-Type' => 'application/json' }
        }
      end
      let(:data) do
        {
          scripts_data: [{ script_url: k6_test_script_k6_test_script_url(k6_test_script, format: :js), script_name: k6_test_script.title }],
          access_key: K6Settings.k6_scripts_external_runner_access_key,
          testing_client_id: K6Settings.k6_scripts_external_runner_testing_client_id }
      end
      let(:runner_response) { double(success?: true) }

      before(:each) do
        allow(HTTParty).to receive(:post).with(runner_endpoint_url, runner_endpoint_params).and_return(runner_response)
      end
      it 'sends request to runner' do
        get :run_k6_script, params: { id: k6_test_script.id }, xhr: true
        expect(HTTParty).to have_received(:post).with(runner_endpoint_url, runner_endpoint_params)
      end
    end
  end

  describe 'GET run_all_k6_scripts' do
    let!(:k6_test_script2) { FactoryBot.create(:k6_test_script) }
    context 'local runner' do
      before(:each) do
        allow_any_instance_of(K6LocalRunnerJob).to receive(:perform).with(anything()).and_return(true)
      end
      it 'calls K6LocalRunnerJob' do
        ActiveJob::Base.queue_adapter = :test
        ActiveJob::Base.queue_adapter.perform_enqueued_jobs = true
        expect {
          get :run_all_k6_scripts, xhr: true
        }.to perform_job(K6LocalRunnerJob)
      end
    end

    context 'remote runner' do
      let!(:k6_settings_instance) { FactoryBot.create(:k6_settings, :external_runner) }
      let(:runner_endpoint_url) { "#{K6Settings.k6_scripts_external_runner_url}#{k6_runner_run_remote_k6_scripts_path(format: :json)}" }
      let(:runner_endpoint_params) do
        {
          body: data.to_json,
          headers: { 'Content-Type' => 'application/json' }
        }
      end
      let(:data) do
        {
          scripts_data: [
                          { script_url: k6_test_script_k6_test_script_url(k6_test_script2, format: :js), script_name: k6_test_script2.title },
                          { script_url: k6_test_script_k6_test_script_url(k6_test_script, format: :js), script_name: k6_test_script.title }
                        ],
          access_key: K6Settings.k6_scripts_external_runner_access_key,
          testing_client_id: K6Settings.k6_scripts_external_runner_testing_client_id }
      end
      let(:runner_response) { double(success?: true) }

      before(:each) do
        allow(HTTParty).to receive(:post).with(runner_endpoint_url, runner_endpoint_params).and_return(runner_response)
      end
      it 'sends request to runner' do
        get :run_all_k6_scripts, xhr: true
        expect(HTTParty).to have_received(:post).with(runner_endpoint_url, runner_endpoint_params)
      end
    end
  end

end
