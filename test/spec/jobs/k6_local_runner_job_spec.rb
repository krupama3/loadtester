require_relative '../rails_helper'

RSpec.describe(K6LocalRunnerJob, type: :job) do

  let(:output_text) { File.read(File.join(Redmine::Plugin.find(:load_tester).directory, 'test/fixtures/example_text_file.txt').to_s) }
  let!(:k6_settings_instance) { FactoryBot.create(:k6_settings, k6_script_access_key: 'access_key') }
  let(:result_json_parser_double) { double('ResultJsonParser', parse: true) }
  let(:script_data) { { script_url: "https://www.example.com/example_script.js", script_name: k6_test_script.title } }
  let(:k6_test_script) { FactoryBot.create(:k6_test_script) }
  let(:output_file_path) { Rails.root.join('public', 'k6_run_results', "#{k6_test_script.title}.txt") }

  subject { described_class.perform_now([script_data]) }

  describe '#perform' do

    before(:each) do
      allow_any_instance_of(K6LocalRunnerJob).to receive(:run_script_with_k6).with(anything(), anything()).and_return(output_text)
      allow_any_instance_of(K6LocalRunnerJob).to receive(:prepare_output_file_name).with(k6_test_script.title).and_return(k6_test_script.title)
      allow(ResultJsonParser).to receive(:new).with(anything()).and_return(result_json_parser_double)
    end

    it 'creates result file' do
      subject
      expect(File.exist?(output_file_path)).to be_truthy
      expect(File.read(output_file_path)).to eq(output_text)
      File.delete(output_file_path) if File.exist?(output_file_path)
    end

  end

end
