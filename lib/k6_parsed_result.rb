# Class represents parsed json file from json test output.
class K6ParsedResult

  attr_reader :path, :thresholds, :metrics, :checks, :vus, :req_failed, :req_statistics, :req_statistics_keys, :req_duration_thresholds

  # @param path [String] file name or parent directory with file name.
  # @example [test_scenario_1v_1s_parsed.json, testing_development/test_scenario_1v_1s_parsed.json]
  def initialize(path)
    @path = path
    file = File.read(results_directory.join(@path))
    @metrics = JSON.parse(file)
    @thresholds = {}
    @req_duration_thresholds = {}
    collect_thresholds
    @checks = @metrics.delete('checks')
    @vus = @metrics.delete('vus')
    @req_failed = @metrics.delete('http_req_failed')
    @req_statistics = @metrics['http_req_duration'].delete('req_statistics')
    @req_statistics_keys = @metrics['http_req_duration'].delete('req_statistics_keys')
  end

  # @return [Float] percentage rate of checks.
  def get_checks_result_percentage
    get_rate_percentage(@checks['data'])
  end

  # @return [String] shows how many checks was completed.
  def get_checks_values
    "#{@checks['data']['non_zero_count'].values.sum}/#{@checks['data']['count']}"
  end

  # @return [Float] percentage rate of data with mectric rate type.
  def get_rate_percentage(data)
    (data['non_zero_count'].values.sum(0.0) / data['count'])*100
  end

  private

  # @return [String] from application root to result directory.
  def results_directory
    Rails.root.join('public', 'k6_run_results')
  end

  # Collect all thresholds to single hash and collect all http_req_duration thresholds to Array of K6Threshold
  def collect_thresholds
    @metrics.each do |metric_name, metric_data|
      metric_data['thresholds'].each do |threshold, result|
        @thresholds["#{metric_name}(#{threshold})"] = result
      end
    end
    @metrics['http_req_duration']['thresholds'].keys.each do |threshold_string|
      threshold = K6Threshold.new(threshold_string)
      @req_duration_thresholds[threshold.left_expression] = threshold.right_expression
    end
  end

end
