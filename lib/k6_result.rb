# Class represents any result output of test.
class K6Result

  attr_reader :name, :path, :timestamp, :script_name, :parent

  # @param result_file_name [String] file name of test output result.
  # @example 20220921112630_find_issue_smoke_test.txt
  def initialize(result_file_name)
    @name = File.basename(result_file_name)
    parent_name = Pathname.new(result_file_name).parent.basename.to_s
    @parent = parent_name == 'k6_run_results' ? '' : parent_name
    @path = self.class.results_directory.join(@parent, @name)
    result_file_name_parts = Pathname.new(@name).sub_ext('').to_s.split('_')
    @timestamp = result_file_name_parts[0].to_i
    last_index = result_file_name_parts.last == 'parsed' ? -2 : -1
    @script_name = result_file_name_parts[1..last_index].join('_')
  end

  # @return [Integer] size of file.
  def size
    exist? ? File.size(path) : 0
  end

  # @return [Boolean].
  def exist?
    File.exist?(path)
  end

  # @return [String] path to result file or link to parsed file.
  def result_link
    if @name.include?('parsed')
      path = @parent.present? ? "#{@parent}/#{@name.gsub('.json', '')}" : "#{@name.gsub('.json', '')}"
      Rails.application.routes.url_helpers.k6_results_display_parsed_result_path(path: path)
    else
      result_link_path
    end
  end

  # @return [String] path to result file.
  def result_link_path
    if @parent.present?
      "/k6_run_results/#{@parent}/#{@name}"
    else
      "/k6_run_results/#{@name}"
    end
  end

  # @return [String] from application root to result directory.
  def self.results_directory
    Rails.root.join('public', 'k6_run_results')
  end

  # @return Array[K6Result] of all result files in result directory.
  def self.all
    Dir.glob(results_directory.join('**/*')).reject { |file| File.directory?(file) }.map { |file| self.new(file) }
  end

end
