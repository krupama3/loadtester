# Class represents threshold of test.
class K6Threshold

  attr_accessor :threshold, :left_expression, :operator, :right_expression

  # @param threshold [String] threshold set by the test.
  # @example "p(99) < 1000"
  def initialize(threshold)
    @threshold = threshold
    split_threshold
  end

  private

  # Splits threshold string into tree parts by operator in the middle
  # Allowed operators are <, >, =.
  def split_threshold
    if @threshold.include?('<')
      expressions = @threshold.split('<')
      @left_expression = expressions.first.strip
      @operator = '<'
      @right_expression = expressions.last.strip
    end
    if @threshold.include?('<=')
      expressions = @threshold.split('<=')
      @left_expression = expressions.first.strip
      @operator = '<='
      @right_expression = expressions.last.strip
    end
    if @threshold.include?('>')
      expressions = @threshold.split('>')
      @left_expression = expressions.first.strip
      @operator = '>'
      @right_expression = expressions.last.strip
    end
    if @threshold.include?('>=')
      expressions = @threshold.split('>=')
      @left_expression = expressions.first.strip
      @operator = '>='
      @right_expression = expressions.last.strip
    end
    if @threshold.include?('=')
      expressions = @threshold.split('=')
      @left_expression = expressions.first.strip
      @operator = '=='
      @right_expression = expressions.last.strip
    end
  end
end
