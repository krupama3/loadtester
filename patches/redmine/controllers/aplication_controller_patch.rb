module LoadTester
  module ApplicationControllerPatch

    def self.included(base)
      base.class_eval do
        before_action :check_rack_mini_profiler,  if: proc { Rails.env.profiling? }

        def check_rack_mini_profiler
          Rack::MiniProfiler.authorize_request
        end
      end
    end
  end
end
ApplicationController.send(:include, LoadTester::ApplicationControllerPatch)
