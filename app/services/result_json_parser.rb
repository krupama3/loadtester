# Parses content of json result file produced by k6 tool
# and creates new parsed file with processed data.
class ResultJsonParser

  # @param result_file_path [String] path to json file with tests results (without .json extension)
  def initialize(result_file_path)
    @result_file_path = result_file_path
    @json_file        = File.read("#{@result_file_path}.json")
    @result_json      = {
      'checks' => { 'type' => 'rate', 'thresholds' => {}, 'raw_data' => {} },
      'vus'    => { 'type' => 'gauge', 'thresholds' => {}, 'raw_data' => {} },
    }
    @req_statistics = {}
  end

  # Parses content of the json file to process required output data.
  #
  # @return [String] file name of parsed data.
  def parse
    @json_file.split("\n").each do |line|
      json_line = JSON.parse(line)
      data      = json_line['data']
      if json_line['type'] == 'Metric'
        parse_metric(data)
      else
        parse_point(data, json_line['metric'])
      end
    end
    process_collected_values
    process_req_statistics
    write_parsed_results_to_file
    output_file_path
  end

  private

  attr_reader :result_file_name, :json_file, :result_json

  # Writes parsed results data to file.
  def write_parsed_results_to_file
    File.open(output_file_path, 'w') do |f|
      f.write(@result_json.to_json)
    end
  end

  # @return [String] file name of parsed data.
  def output_file_path
    "#{@result_file_path}_parsed.json"
  end

  # Process values gathered from json file by metric type.
  def process_collected_values
    @result_json.values.each do |metric_data|
      case metric_data['type']
      when 'trend'
        LoadTesterMetricTypes::Trend.new(metric_data).process_raw_data
      when 'rate'
        LoadTesterMetricTypes::Rate.new(metric_data).process_raw_data
      when 'gauge'
        LoadTesterMetricTypes::Gauge.new(metric_data).process_raw_data
      when 'counter'
        LoadTesterMetricTypes::Counter.new(metric_data).process_raw_data
      end
    end
  end

  # Process http_req_duration values gathered from json file.
  def process_req_statistics
    if @result_json['http_req_duration'].blank?
      @result_json['http_req_duration'] = {}
      statistics_parser = ReqStatisticsParser.new(@req_statistics, [])
    else
      statistics_parser = ReqStatisticsParser.new(@req_statistics, @result_json['http_req_duration']['thresholds'].keys)
    end
    @result_json['http_req_duration']['req_statistics'] = statistics_parser.parse
    @result_json['http_req_duration']['req_statistics_keys'] = statistics_parser.keys
  end

  # Process one line of json file with type: metric.
  def parse_metric(data)
    if data['thresholds'].present?
      thresholds = {}
      data['thresholds'].map { |threshold| thresholds[threshold] = '' }
      @result_json[data['name']] = {
        'type' => data['type'], 'thresholds' => thresholds, 'raw_data' => {}
      }
    end
  end

  # Process one line of json file with type: point.
  def parse_point(data, metric)
    if @result_json[metric].present?
      time = Time.parse(data['time']).to_formatted_s(:db)
      @result_json[metric]['raw_data'][time] = [] unless @result_json[metric]['raw_data'][time].present?
      @result_json[metric]['raw_data'][time] << data['value']
    end
    collect_req_statistics(data) if metric == 'http_req_duration'
  end

  # Gather statistics data from one http_req_duration line of json file.
  def collect_req_statistics(data)
    name = data['tags']['name']
    method = data['tags']['method']
    prepare_statistics_headers(name, method)
    @req_statistics[name][method]['raw_data'] << data['value']
  end

  # Adds empty hash to @req_statistics if they not exist to prevent errors.
  def prepare_statistics_headers(name, method)
    @req_statistics[name] = {} if @req_statistics[name].blank?
    @req_statistics[name][method] = { 'raw_data' => [] } if @req_statistics[name][method].blank?
  end
end
