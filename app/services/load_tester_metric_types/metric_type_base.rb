module LoadTesterMetricTypes
  # Parent class for metric types.
  class MetricTypeBase

    # @param metric_data [Hash] metric data gathered from json.
    def initialize(metric_data)
      @metric_data = metric_data
      @raw_data = metric_data.delete("raw_data")
      @thresholds = @metric_data['thresholds'].keys.map { |threshold| K6Threshold.new(threshold) }
      @metric_data['data'] = {}
    end

    protected

    # Process all raw data from input metric_data.
    # @raise [NotImplementedError] due to every metric type needs own way of procession
    def process_raw_data
      raise NotImplementedError
    end

    # Evaluates if value(left_expression) meets threshold criteria.
    #
    # @param value [Float] value parsed from results.
    # @param operator [String] metric data gathered from json
    # @param threshold_value [Float] threshold value set by test.
    # @return [Boolean].
    def threshold_satisfied?(value, operator, threshold_value)
      case operator
      when '<'
        value < threshold_value
      when '<='
        value <= threshold_value
      when '>'
        value > threshold_value
      when '>='
        value >= threshold_value
      when '=='
        value == threshold_value
      else
        false
      end
    end

    private

    # Evaluates all thresholds from input metric_data.
    # @raise [NotImplementedError] due to every metric type needs own way of evaluation
    def evaluate_thresholds
      raise NotImplementedError
    end

  end
end
