module LoadTesterMetricTypes
  # Process data gathered from json with metric type gauge.
  class Gauge < MetricTypeBase

    # @param metric_data [Hash] metric data gathered from json.
    def initialize(metric_data)
      super
      @metric_data['data']['last_value'] = {}
      @metric_data['data']['min'] = {}
      @metric_data['data']['max'] = {}
    end

    # Process all raw data from input metric_data.
    # Method modifies input object metric_data.
    def process_raw_data
      @raw_data.each do |key, value|
        @metric_data['data']['last_value'][key] = value.last
        @metric_data['data']['min'][key] = value.min
        @metric_data['data']['max'][key] = value.max
      end
      @metric_data['data']['min'] = @metric_data['data']['min'].values.min
      @metric_data['data']['max'] = @metric_data['data']['max'].values.max
      evaluate_thresholds unless @thresholds.empty?
    end

    private

    # Evaluates all thresholds from input metric_data.
    def evaluate_thresholds
      @thresholds.each do |threshold|
        if threshold.left_expression == 'value'
          value = %w[< <=].include? threshold.threshold.operator ? @metric_data['data']['max'] : @metric_data['data']['min']
          @metric_data['thresholds'][threshold.threshold] = threshold_satisfied?(value, threshold.operator, threshold.right_expression.to_f)
        else
          @metric_data['thresholds'][threshold.threshold] = false
        end
      end
    end

  end
end
