module LoadTesterMetricTypes
  # Process data gathered from json with metric type rate.
  class Rate < MetricTypeBase

    # @param metric_data [Hash] metric data gathered from json.
    def initialize(metric_data)
      super
      @metric_data['data']['non_zero_count'] = {}
      @metric_data['data']['count'] = 0
      @all_non_zero_count = 0
    end

    # Process all raw data from input metric_data.
    # Method modifies input object metric_data.
    def process_raw_data
      @raw_data.each do |key, value|
        sum = value.sum
        @metric_data['data']['non_zero_count'][key] = sum
        @metric_data['data']['count'] += value.size
        @all_non_zero_count += sum
      end
      evaluate_thresholds unless @thresholds.empty?
    end

    private

    # Evaluates all thresholds from input metric_data.
    def evaluate_thresholds
      @thresholds.each do |threshold|
        if threshold.left_expression == "rate"
          rate = @all_non_zero_count.to_f / @metric_data['data']['count']
          @metric_data['thresholds'][threshold.threshold] = threshold_satisfied?(rate, threshold.operator, threshold.right_expression.to_f)
        else
          @metric_data['thresholds'][threshold.threshold] = false
        end
      end
    end

  end
end
