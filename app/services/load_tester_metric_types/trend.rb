module LoadTesterMetricTypes
  # Process data gathered from json with metric type trend.
  class Trend < MetricTypeBase

    # @param metric_data [Hash] metric data gathered from json.
    def initialize(metric_data)
      super
      @metric_data['data']['avg'] = {}
      @metric_data['data']['min'] = {}
      @metric_data['data']['max'] = {}
      @all_values = []
      @all_values.extend(DescriptiveStatistics)
    end

    # Process all raw data from input metric_data.
    # Method modifies input object metric_data.
    def process_raw_data
      @raw_data.each do |key, value|
        @metric_data['data']['avg'][key] = (value.sum(0.0) / value.size.to_f).round(2)
        @metric_data['data']['min'][key] = value.min.round(2)
        @metric_data['data']['max'][key] = value.max.round(2)
        @all_values.push(*value)
      end
      evaluate_thresholds unless @thresholds.empty?
    end

    private

    # Evaluates all thresholds from input metric_data.
    def evaluate_thresholds
      @thresholds.each do |threshold|
        case threshold.left_expression
        when 'avg'
          result = (@all_values.sum(0.0) / @all_values.size.to_f).round(2)
          @metric_data['thresholds'][threshold.threshold] = threshold_satisfied?(result, threshold.operator, threshold.right_expression.to_f)
        when 'max'
          result = @all_values.max&.round(2)
          @metric_data['thresholds'][threshold.threshold] = threshold_satisfied?(result, threshold.operator, threshold.right_expression.to_f)
        when 'min'
          result = @all_values.min&.round(2)
          @metric_data['thresholds'][threshold.threshold] = threshold_satisfied?(result, threshold.operator, threshold.right_expression.to_f)
        when 'med'
          result = @all_values.median.round(2)
          @metric_data['thresholds'][threshold.threshold] = threshold_satisfied?(result, threshold.operator, threshold.right_expression.to_f)
        else
          if threshold.left_expression.match("^p[(](100|[0-9]{1,2})[)]$")
            percentile = threshold.left_expression.gsub('p(', '').gsub(')', '').to_f
            result = @all_values.percentile(percentile).round(2)
            @metric_data['thresholds'][threshold.threshold] = threshold_satisfied?(result, threshold.operator, threshold.right_expression.to_f)
          else
            @metric_data['thresholds'][threshold.threshold] = false
          end
        end

      end
    end

  end
end
