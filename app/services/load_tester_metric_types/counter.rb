module LoadTesterMetricTypes
  # Process data gathered from json with metric type counter.
  class Counter < MetricTypeBase

    # @param metric_data [Hash] metric data gathered from json.
    def initialize(metric_data)
      super
      @metric_data['data']['sum'] = 0
      @metric_data['data']['count'] = 0
    end

    # Process all raw data from input metric_data.
    # Method modifies input object metric_data.
    def process_raw_data
      @raw_data.each do |_key, value|
        @metric_data['data']['sum'] += value.sum
        @metric_data['data']['count'] += value.size
      end
      evaluate_thresholds unless @thresholds.empty?
    end

    private

    # Evaluates all thresholds from input metric_data.
    def evaluate_thresholds
      @thresholds.each do |threshold|
        case threshold.left_expression
        when 'count'
          @metric_data['thresholds'][threshold.threshold] = threshold_satisfied?(@metric_data['data']['count'], threshold.operator, threshold.right_expression.to_f)
        when 'rate'
          rate = @metric_data['data']['sum'].to_f / @metric_data['data']['count']
          @metric_data['thresholds'][threshold.threshold] = threshold_satisfied?(rate, threshold.operator, threshold.right_expression.to_f)
        else
          @metric_data['thresholds'][threshold.threshold] = false
        end
      end
    end

  end
end
