# Parses http request duration statistics raw data.
class ReqStatisticsParser

  # @param req_statistics [Hash] gathered raw http request duration statistics.
  # @param thresholds [Array<String>] gathered http request duration thresholds.
  def initialize(req_statistics, thresholds)
    @req_statistics = req_statistics
    @thresholds = []
    @additional_keys = []
    @result = {}
    thresholds.each do |threshold|
      k6_threshold = K6Threshold.new(threshold)
      @thresholds << k6_threshold
      @additional_keys << k6_threshold.left_expression
    end
  end

  # Parses content of raw http request duration statistics.
  #
  # @return [Hash] parsed http request duration statistics.
  def parse
    process_statistics
    @result
  end

  # @return [Array<String>] names of http request duration statistics.
  def keys
    ['count', 'avg', 'min', 'max', 'p(90)', 'p(95)'] + @additional_keys
  end

  private

  # Process content of raw http request duration statistics.
  def process_statistics
    @req_statistics.each do |name, req_data|
      @result[name] = {} if @result[name].blank?
      req_data.each do |method, method_data|
        @result[name][method] = process_request(method_data['raw_data'])
      end
    end
  end

  # Process http request duration statistics raw data per request.
  #
  # @param raw_data [Array<float>] values of all duration times of http request.
  # @return [Hash] processed http request duration statistics data.
  def process_request(raw_data)
    request_statistics = {}
    keys.each do |key|
      request_statistics[key] = process_data_by_key(raw_data, key)
    end
    request_statistics
  end

  # Process http request duration statistics raw data by key.
  #
  # @param values [Array<float>] values of all duration times of http request.
  # @param key [String] defines the way of processing values and output value.
  # @return [Integer | Float] float value calculated from values by specific key.
  def process_data_by_key(values, key)
    case key
    when 'count'
      values.size
    when 'avg'
      (values.sum(0.0) / values.size.to_f).round(2)
    when 'min'
      values.min.round(2)
    when 'max'
      values.max.round(2)
    else
      if key.match("^p[(](100|[0-9]{1,2})[)]$")
        values.extend(DescriptiveStatistics)
        percentile = key.gsub('p(', '').gsub(')', '').to_f
        values.percentile(percentile).round(2)
      end
    end
  end
end
