class K6TestingClient < ActiveRecord::Base
  include Redmine::SafeAttributes

  validates :name, :web_url, :script_access_key, presence: true
  validates :name, :internal_name, uniqueness: true

  before_validation :set_internal_name, if: proc { name_changed? }

  scope :sorted, lambda { order(:position) }

  acts_as_positioned

  safe_attributes(
    'name',
    'web_url',
    'script_access_key'
  )

  def set_internal_name
    self.internal_name = name.parameterize.underscore
  end

end
