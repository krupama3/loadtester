class K6TestScenario < K6TestConfiguration

  has_many :k6_test_scrips, inverse_of: :k6_test_scenario
  has_many :k6_test_types, through: :k6_test_scrips

  def editable?
    !system_configuration?
  end

  def deletable?
    !system_configuration?
  end

  def self.system_scenarios_names
    %w[login log_time check_project find_issue update_issue wide_redmine_functionality]
  end
end
