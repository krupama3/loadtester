class K6Settings < ActiveRecord::Base

  # validate only one value for singleton_guard (with combination of column uniqueness provide singleton pattern)
  validates :singleton_guard, inclusion: { in: [0] }

  before_update :confirm_first_visit

  def self.instance
    first_or_create!(singleton_guard: 0)
  end

  def self.k6_scripts_api_key
    instance.k6_scripts_api_key
  end

  def self.k6_scripts_base_url
    instance.k6_scripts_base_url
  end

  def self.k6_external_runner
    instance.k6_external_runner
  end

  def self.k6_scripts_external_runner_url
    instance.k6_scripts_external_runner_url
  end

  def self.k6_scripts_external_runner_access_key
    instance.k6_scripts_external_runner_access_key
  end

  def self.k6_scripts_external_runner_testing_client_id
    instance.k6_scripts_external_runner_testing_client_id
  end

  def self.k6_script_access_key
    instance.k6_script_access_key
  end

  def self.k6_runner_access_key
    instance.k6_runner_access_key
  end

  # def self.k6_script_output_format
  #   instance.k6_script_output_format
  # end

  def self.k6_prometheus
    instance.k6_prometheus
  end

  def self.k6_prometheus_user
    instance.k6_prometheus_user
  end

  def self.k6_prometheus_password
    instance.k6_prometheus_password
  end

  def self.k6_prometheus_remote_url
    instance.k6_prometheus_remote_url
  end

  def self.visited
    instance.visited
  end

  def self.permitable_attributes
    %i[k6_scripts_api_key k6_scripts_base_url k6_external_runner k6_scripts_external_runner_url
     k6_scripts_external_runner_access_key k6_script_access_key k6_runner_access_key k6_prometheus
     k6_prometheus_user k6_prometheus_password k6_prometheus_remote_url k6_scripts_external_runner_testing_client_id]
  end

  def confirm_first_visit
    return if visited

    self.visited = true
  end

end
