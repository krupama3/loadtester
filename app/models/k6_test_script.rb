class K6TestScript < ActiveRecord::Base
  include Redmine::SafeAttributes

  belongs_to :k6_test_scenario
  belongs_to :k6_test_type

  validates :title, :k6_test_scenario_id, :k6_test_type_id, presence: true

  scope :sorted, lambda { order(:position) }

  acts_as_positioned

  safe_attributes(
    'title',
    'k6_test_scenario_id',
    'k6_test_type_id',
    'position'
  )

end
