class K6TestType < K6TestConfiguration

  has_many :k6_test_scrips, inverse_of: :k6_test_type
  has_many :k6_test_scenarios, through: :k6_test_scrips

end
