class K6TestConfiguration < ActiveRecord::Base
  self.table_name = :k6_test_configurations
  include Redmine::SafeAttributes

  unless ActiveRecord::Base.connection.supports_json?
    serialize :k6_script_configuration, JSON
  end

  SUBCLASS_NAMES = %w[
    k6_test_scenario
    k6_test_type
  ].freeze

  validates :title, presence: true
  validate :k6_script_configuration_json_format

  scope :sorted, lambda { order(:position) }

  acts_as_positioned

  safe_attributes(
    'title',
    'k6_script_configuration',
    'position'
  )


  def k6_script_configuration_json_format
    return true unless k6_script_configuration.present?

    JSON.parse(k6_script_configuration).all?
  rescue JSON::ParserError
    errors.add(:k6_script_configuration, :invalid, message: "Field is invalid json format.")
  end

end
