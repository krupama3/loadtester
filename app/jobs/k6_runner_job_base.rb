class K6RunnerJobBase < ActiveJob::Base

  attr_accessor :results_dir, :local_script_path

  LOCAL_K6_SCRIPT_NAME = 'k6_script.js'.freeze

  # Run script with k6 tool with default output.
  #
  # @param script_url [String] web url of the script.
  # @param output_file_path [String] file path for json output (without .json extension).
  # @return [String] test results in text form.
  def run_script_with_default(script_url, output_file_path)
    `k6 run --out json=#{output_file_path}.json #{resolve_script_by_url(script_url)}`
  end

  # Run script with k6 tool.
  #
  # @param script_url [String] web url of the script.
  # @param output_file_path [String] file path for json output (without .json extension).
  # @return [String] test results in text form.
  def run_script_with_k6(script_url, output_file_path)
    if K6Settings.k6_prometheus
      run_script_with_prometheus script_url, output_file_path
    else
      run_script_with_default script_url, output_file_path
    end
  end

  # Run script with k6 tool with default and prometheus DB output.
  #
  # @param script_url [String] web url of the script.
  # @param output_file_path [String] file path for json output (without .json extension).
  # @return [String] test results in text form.
  def run_script_with_prometheus(script_url, output_file_path)
    `#{prometheus_configuration} k6 run --out json=#{output_file_path}.json  #{resolve_script_by_url(script_url)} --out output-prometheus-remote`
  end

  # @return [String] prometheus configuration for run script command.
  def prometheus_configuration
    "K6_PROMETHEUS_USER=#{K6Settings.k6_prometheus_user} K6_PROMETHEUS_PASSWORD=#{K6Settings.k6_prometheus_password} K6_PROMETHEUS_REMOTE_URL=#{K6Settings.k6_prometheus_remote_url}"
  end

  # @param script_name [String] name of test script.
  # @return [String] uniq file_name for test outputs.
  def prepare_output_file_name(script_name)
    "#{Time.now.to_formatted_s(:number)}_#{script_name}"
  end

  # @param output_file_name [String] name of output file.
  # @return [String] full path for output file_name.
  def output_file_path(output_file_name)
    "#{results_dir}/#{output_file_name}"
  end

  # Setups result directory path and creates directory if not exists.
  #
  # @return [String] to result directory path.
  def k6_run_results_directory
    dir = Rails.root.join('public', 'k6_run_results')
    Dir.mkdir(dir) unless Dir.exist?(dir)
    dir
  end

  # Setups temporary script directory path and creates directory if not exists.
  #
  # @return [String] to temporary script directory path.
  def k6_run_local_script_directory
    dir = Rails.root.join('tmp', 'k6_run_local_scripts')
    Dir.mkdir(dir) unless Dir.exist?(dir)
    dir
  end

  # Process script url, if url has https protocol returns url and let k6 tool to get script from url.
  # Otherwise download script from url saves it locally and provide file path for k6 tool.
  #
  # @param script_url [String] web url of the script.
  # @return [String] url or path to local script.
  def resolve_script_by_url(script_url)
    return script_url if script_url.include?('https://')

    create_local_script_file(script_url)
    local_script_path
  end

  # Downloads script from url and saves it locally to local_script_path.
  #
  # @param script_url [String] web url of the script.
  def create_local_script_file(script_url)
    `wget -O #{local_script_path} #{script_url}`
  end

end
