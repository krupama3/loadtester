class K6LocalRunnerJob < K6RunnerJobBase

  # Sets jobs instance variables to be available in all methods.
  before_perform do |job|
    job.results_dir = k6_run_results_directory
    job.local_script_path = "#{k6_run_results_directory}/#{LOCAL_K6_SCRIPT_NAME}"
  end

  # Performs actions to run script requested locally.
  #
  # @param scripts_data [Array<Hash>] script data required to run script.
  def perform(scripts_data)
    return unless scripts_data.present?

    scripts_data.each do |script_data|
      output_file_name = prepare_output_file_name(script_data[:script_name])
      output_file_path = "#{results_dir}/#{output_file_name}"

      test_result = run_script_with_k6(build_script_url_with_access_key(script_data[:script_url]), output_file_path)

      File.open("#{output_file_path(output_file_name)}.txt", 'w') do |file|
        file.write(test_result)
      end

      File.delete(local_script_path) if File.exist?(local_script_path)

      ResultJsonParser.new(output_file_path).parse
    end
  end

  # Adds application access key as parameter to script_url to get access to script.
  #
  # @param script_url [String] web url of the script.
  # @return [String] url of script extended by access key.
  def build_script_url_with_access_key(script_url)
    "#{script_url}?#{{ access_key: K6Settings.k6_script_access_key }.to_query}"
  end

end
