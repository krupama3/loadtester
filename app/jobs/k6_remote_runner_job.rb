class K6RemoteRunnerJob < K6RunnerJobBase
  require 'httparty'

  attr_accessor :testing_client

  # Sets jobs instance variables to be available in all methods.
  before_perform do |job|
    job.testing_client = job.arguments.second
    job.results_dir = k6_run_results_directory
    job.local_script_path = "#{k6_run_results_directory}/#{LOCAL_K6_SCRIPT_NAME}"
  end


  # Performs actions to run script requested by testing client.
  #
  # @param scripts_data [Array<Hash>] script data required to run script.
  # @param testing_client [K6TestingClient].
  def perform(scripts_data, testing_client)
    return unless scripts_data.present?
    return unless testing_client.present?

    scripts_data.each do |script_data|
      output_file_name = prepare_output_file_name(script_data[:script_name])
      output_file_path = "#{results_dir}/#{output_file_name}"

      test_result = run_script_with_k6(build_script_url_with_access_key(script_data[:script_url]), output_file_path)

      File.open("#{output_file_path(output_file_name)}.txt", 'w') do |file|
        file.write(test_result)
      end

      File.delete(local_script_path) if File.exist?(local_script_path)

      parsed_json_file_path = ResultJsonParser.new(output_file_path).parse

      data = {
        access_key: K6Settings.k6_runner_access_key,
        json_file: File.read(parsed_json_file_path),
        json_file_name: "#{output_file_name}_parsed.json",
        text_file: test_result,
        text_file_name: "#{output_file_name}.txt",
        script_name: script_data[:script_name]
      }
      post_results(testing_client.web_url, data)
    end
  end

  # Send post request with result output files back to testing client.
  #
  # @param url [String] web url of testing client.
  # @param data [Hash] result data.
  def post_results(url, data)
    HTTParty.post("#{url}#{receiver_path}", body: data)
  end

  # @return [String] path of endpoint receiving results data.
  def receiver_path
    Rails.application.routes.url_helpers.k6_results_retrieve_k6_results_path(format: :json)
  end

  # Overrides parent method, setups result directory path and creates directory if not exists.
  #
  # @return [String] to result directory path.
  def k6_run_results_directory
    dir = super
    return dir unless testing_client&.internal_name

    dir = Rails.root.join(dir, testing_client&.internal_name)
    Dir.mkdir(dir) unless Dir.exist?(dir)
    dir
  end

  # Overrides parent method, setups temporary script directory path and creates directory if not exists.
  #
  # @return [String] to temporary script directory path.
  def k6_run_local_script_directory
    dir = super
    return dir unless testing_client&.internal_name

    dir = Rails.root.join(dir, testing_client&.internal_name)
    Dir.mkdir(dir) unless Dir.exist?(dir)
    dir
  end

  # Adds testing client access key as parameter to script_url to get access to script.
  #
  # @param script_url [String] web url of the script.
  # @return [String] url of script extended by access key.
  def build_script_url_with_access_key(script_url)
    "#{script_url}?#{{ access_key: testing_client&.script_access_key }.to_query}"
  end

end
