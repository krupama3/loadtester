import http from 'k6/http';
import { check, group, sleep, fail } from 'k6';

export const options = {
    "vus": 1,
    "duration": "1m",
    "thresholds": {
        "http_req_duration": ["p(99) \u003c 1000"]
    }
};

const BASE_URL = 'http://localhost:3000';

export default () => {

    const myObjects = http.get(`${BASE_URL}/projects.json`).json();
    check(myObjects, { 'retrieved projects': (obj) => obj['projects'].length > 0 });

    const projectId = myObjects['projects'][0]['id'];
    const myProject = http.get(`${BASE_URL}/projects/${projectId}.json`).json();
    check(myProject, { 'retrieved project': (obj) => obj['project']['id'] === projectId });

    const activityResp = http.get(`${BASE_URL}/projects/${projectId}/activity`);
    check(activityResp, { 'status was 200': (r) => r.status === 200 });

    sleep(1);
};

