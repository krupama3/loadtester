import http from 'k6/http';
import { check, sleep } from 'k6';

export const options = {
    "vus": 1,
    "duration": "2s",
    "thresholds": {
        "http_req_duration": ["p(99) \u003c 1000"]
    }
};

const BASE_URL = 'http://localhost:3000';
const API_KEY = 'd59f4698b57f7e34757118a41c7c3dca82fb1d23';

export default () => {

    const params = {
        headers: {
            'X-Redmine-API-Key': API_KEY,
        },
    };

    const logged = http.get(`${BASE_URL}/login`, params);
    check(logged, { 'logged in successfully': (r) => r.status === 200 });

    sleep(1);
};
