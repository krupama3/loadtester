const responses = http.batch([
    ['GET', `${BASE_URL}/projects/1/`, null, {tags: {name: 'Project'}}],
    ['GET', `${BASE_URL}/issues/1/`, null, {tags: {name: 'Issues'}}],
    ['GET', `${BASE_URL}/projects/2/`, null, {tags: {name: 'Project'}}],
    ['GET', `${BASE_URL}/issues/2/`, null, {tags: {name: 'Issues'}}],
]);

sleep(1);
