import http from 'k6/http';
import { check, sleep } from 'k6';

// export default function () {
//     const apiKey = 'd59f4698b57f7e34757118a41c7c3dca82fb1d23';
//
//     const url = 'http://localhost:3000/my/page';
//
//     const params = {
//         headers: {
//             'X-Redmine-API-Key': apiKey,
//         },
//     };
//
//     const res = http.get(url, params);
//     console.log('status: ' + res.status)
//     check(res, { 'status was 200': (r) => r.status == 200 });
//
//     sleep(1);
// }

// export default function () {
//     const apiKey = 'd59f4698b57f7e34757118a41c7c3dca82fb1d23';
//
//     const url = 'http://localhost:3000/login';
//
//     const params = {
//         headers: {
//             'X-Redmine-API-Key': apiKey,
//         },
//     };
//
//     const res = http.get(url, params);
//     console.log('status: ' + res.status)
//     check(res, { 'status was 200': (r) => r.status == 200 });
//
//     sleep(1);
// }

export default function () {
    let res = http.get('http://localhost:3000/login');

    res = res.submitForm({
        formSelector: 'form',
        fields: { username: 'admin', password: 'diplomka' },
    });
    check(res, { 'status was 200': (r) => r.status == 200 });
}
