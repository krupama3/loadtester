const params = {
    headers: {
        'X-Redmine-API-Key': API_KEY,
    },
};

const logged = http.get(`${BASE_URL}/login`, params);
check(logged, {'logged in successfully': (r) => r.status === 200});

sleep(1);
