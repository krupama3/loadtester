const params = {
    headers: {
        'X-Redmine-API-Key': API_KEY,
    },
};

const logged = http.get(`${BASE_URL}/login`, params);
check(logged, {'logged in successfully': (r) => r.status === 200});

const issues = http.get(`${BASE_URL}/issues.json`, params).json();
check(issues, {'retrieved issues': (obj) => obj['issues'].length > 0});
const issueId = issues['issues'][0]['id'];

const issue = http.get(`${BASE_URL}/issues/${issueId}.json`, params).json();
check(issue, {'retrieved issue': (obj) => obj['issue']['id'] === issueId});

const timeEntryForm = http.get(`${BASE_URL}/issues/${issueId}/time_entries/new`, params);
check(timeEntryForm, {'time entry form loaded successfully': (r) => r.status === 200});

let timeEntryData = {time_entry: {issue_id: 1, user_id: 1, hours: 0.5, comments: 'Moj vykaz', activity_id: 1}}
params['headers']['Content-Type'] = 'application/json'

const createdTimeEntry = http.post(`${BASE_URL}/time_entries.json`, JSON.stringify(timeEntryData), params);
check(createdTimeEntry, {'time entry created successfully': (r) => r.status === 201});

sleep(1);
