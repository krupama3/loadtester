import http from 'k6/http';
import { check, group, sleep } from 'k6';

export const options = {
    stages: [
        { duration: '5m', target: 100 }, // simulate ramp-up of traffic from 1 to 100 users over 5 minutes.
        { duration: '10m', target: 100 }, // stay at 100 users for 10 minutes
        { duration: '5m', target: 0 }, // ramp-down to 0 users
    ],
    thresholds: {
        'http_req_duration': ['p(99)<1500'], // 99% of requests must complete below 1.5s
        // 'logged in successfully': ['p(99)<1500'], // 99% of requests must complete below 1.5s
    },
};

const BASE_URL = 'http://localhost:3000';
// const USERNAME = 'TestUser';
// const PASSWORD = 'SuperCroc2020';

export default () => {
    const myObjects = http.get(`${BASE_URL}/projects.json`).json();
    check(myObjects, { 'retrieved projects': (obj) => obj['projects'].length > 0 });

    const projectId = myObjects['projects'][0]['id'];
    const myProject = http.get(`${BASE_URL}/projects/${projectId}.json`).json();
    check(myProject, { 'retrieved project': (obj) => obj['project']['id'] === projectId });

    const activityResp = http.get(`${BASE_URL}/projects/${projectId}/activity`);
    check(activityResp, { 'status was 200': (r) => r.status === 200 });

    sleep(1);
};
