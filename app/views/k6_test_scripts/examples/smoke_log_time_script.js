import http from 'k6/http';
import { check, sleep } from 'k6';

export const options = {
    "vus": 1,
    "duration": "2s",
    "thresholds": {
        "http_req_duration": ["p(99) \u003c 1000"]
    }
};

const BASE_URL = 'http://localhost:3000';
const API_KEY = 'd59f4698b57f7e34757118a41c7c3dca82fb1d23';

export default () => {

    const params = {
        headers: {
            'X-Redmine-API-Key': API_KEY,
        },
    };

    const logged = http.get(`${BASE_URL}/login`, params);
    check(logged, { 'logged in successfully': (r) => r.status === 200 });

    const issues = http.get(`${BASE_URL}/issues.json`, params).json();
    check(issues, { 'retrieved issues': (obj) => obj['issues'].length > 0 });

    const issueId = issues['issues'][0]['id'];
    const issue = http.get(`${BASE_URL}/issues/${issueId}.json`, params).json();
    check(issue, { 'retrieved issue': (obj) => obj['issue']['id'] === issueId });

    const timeEntryForm = http.get(`${BASE_URL}/issues/${issueId}/time_entries/new`, params);
    check(timeEntryForm, { 'time entry form loaded successfully': (r) => r.status === 200 });

    let timeEntryData = { time_entry: { issue_id: 1, user_id: 1, hours: 0.5, comments: 'Moj vykaz', activity_id: 4 } }
    params['headers']['Content-Type'] = 'application/json'

    const createdTimeEntry = http.post(`${BASE_URL}/time_entries.json`, JSON.stringify(timeEntryData) ,params);
    check(createdTimeEntry, { 'time entry created successfully': (r) => r.status === 201 });

    sleep(1);
};

// const USERNAME = 'admin';
// const PASSWORD = 'diplomka';
// export default () => {
//     let res = http.get(`${BASE_URL}/login`);
//
//     res = res.submitForm({
//         formSelector: 'form',
//         fields: { username: USERNAME, password: PASSWORD }
//     });
//     check(res, { 'logged in successfully': (r) => r.status === 200 });
//
//     const time_entry_edit = http.get(`${BASE_URL}/issues/1/time_entries/new`);
//     check(time_entry_edit, { 'time entry form loaded successfully': (r) => r.status === 200 });
//
//     res = time_entry_edit.submitForm({
//         formSelector: 'form',
//         fields: { issue_id: 1, user_id: 1, hours: 0.5, comments: 'Moj vykaz', activity_id: 1 }
//     });
//     check(res, { 'time entry created successfully': (r) => r.status === 200 });
//
//     sleep(1);
// };
