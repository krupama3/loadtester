const myObjects = http.get(`${BASE_URL}/projects.json`).json();
check(myObjects, {'retrieved projects': (obj) => obj['projects'].length > 0});

const projectId = myObjects['projects'][0]['id'];
const myProject = http.get(`${BASE_URL}/projects/${projectId}.json`).json();
check(myProject, {'retrieved project': (obj) => obj['project']['id'] === projectId});

const activityResp = http.get(`${BASE_URL}/projects/${projectId}/activity`);
check(activityResp, {'status was 200': (r) => r.status === 200});

sleep(1);

