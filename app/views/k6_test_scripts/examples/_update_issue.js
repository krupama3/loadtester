const params = {
    headers: {
        'X-Redmine-API-Key': API_KEY,
    },
};

const logged = http.get(`${BASE_URL}/login`, params);
check(logged, {'logged in successfully': (r) => r.status === 200});

const issues = http.get(`${BASE_URL}/issues.json`, params).json();
check(issues, {'retrieved issues': (obj) => obj['issues'].length > 0});

const issueId = issues['issues'][0]['id'];
const issue = http.get(`${BASE_URL}/issues/${issueId}.json`, params).json();
check(issue, {'retrieved issue': (obj) => obj['issue']['id'] === issueId});

const issueForm = http.get(`${BASE_URL}/issues/${issueId}/edit`, params);
check(issueForm, {'issue form loaded successfully': (r) => r.status === 200});

let issueData = {issue: {done_ratio: 75}}
params['headers']['Content-Type'] = 'application/json'

const updatedIssue = http.put(`${BASE_URL}/issues/1.json`, JSON.stringify(issueData), params);
check(updatedIssue, {'issue updated successfully': (r) => r.status === 200});

sleep(1);
