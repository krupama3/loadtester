import http from 'k6/http';
import { check, sleep } from 'k6';

export const options = {"stages":[{"duration":"10s","target":100},{"duration":"1m","target":100},{"duration":"10s","target":0}],"thresholds":{"http_req_duration":["p(99) \u003c 1500"]}};

const BASE_URL = 'http://localhost:3000';
const API_KEY = 'd59f4698b57f7e34757118a41c7c3dca82fb1d23';

export default () => {

    const params = {
        headers: {
            'X-Redmine-API-Key': API_KEY,
        },
    };

    const res = http.get(`${BASE_URL}/login`, params);
    check(res, {'logged in successfully': (r) => r.status === 200});

    const issues = http.get(`${BASE_URL}/issues.json`, params).json();
    check(issues, {'retrieved issues': (obj) => obj['issues'].length > 0});

    const issueId = issues['issues'][0]['id'];
    const issue = http.get(`${BASE_URL}/issues/${issueId}.json`, params).json();
    check(issue, {'retrieved issue': (obj) => obj['issue']['id'] === issueId});

    const spentTime = http.get(`${BASE_URL}/issues/${issueId}?tab=time_entries`, params);
    check(spentTime, {'spent time checked successfully': (r) => r.status === 200});

    sleep(1);

};
