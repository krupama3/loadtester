import http from 'k6/http';
import { sleep } from 'k6';

export const options = {
    stages: [
        { duration: '2s', target: 4 },
        { duration: '10s', target: 4 },
        { duration: '2s', target: 0 }
    ],
};

const BASE_URL = 'http://localhost:3000'

export default () => {

    const responses = http.batch([
        ['GET', `${BASE_URL}/projects/1/`, null, { tags: { name: 'Project' } }],
        ['GET', `${BASE_URL}/issues/1/`, null, { tags: { name: 'Issues' } }],
        ['GET', `${BASE_URL}/projects/2/`, null, { tags: { name: 'Project' } }],
        ['GET', `${BASE_URL}/issues/2/`, null, { tags: { name: 'Issues' } }],
    ]);

    sleep(1);
}
