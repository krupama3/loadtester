module K6ResultsHelper

  # Method returns css class by evaluation its value with thresholds.
  #
  # @param column [String] column name.
  # @param value [Float] table cell value.
  # @param thresholds [Array<K6Threshold>].
  # @return [String] css class for specific table cell in column
  def k6_parsed_result_table_color_class(column, value, thresholds)
    red_text_threshold = 2000
    orange_text_threshold = 1000
    if thresholds.keys.include?(column)
      red_text_threshold = thresholds[column].to_i
      orange_text_threshold = thresholds[column].to_i * 0.8
    end

    return "ok_req_duration" if value < orange_text_threshold
    return "warning_req_duration" if value < red_text_threshold

    "error_req_duration"
  end

end
