module K6TestScriptsHelper

  # @param postfix [String] last part of endpoint url
  # @return [String] url
  def build_url(postfix)
    "#{@base_url}#{postfix}"
  end

  # Builds request params for api request from request configuration.
  #
  # @param request_config [Hash] request configuration.
  # @return [JSON]
  def build_api_request_params(request_config)
    params = request_config['params'].presence || {}
    params['headers'] = if params['headers'].present?
                          @default_api_headers.merge(parse_headers(params['headers']))
                        else
                          @default_api_headers
                        end
    params.to_json
  end

  # def build_html_request_params(request_config)
  #   params = request_config['params'].presence || {}
  #   if params['tags'].present?
  #     params['tags']['url'] = build_url(params['tags']['url']) if params['tags']['url'].present?
  #   end
  #   params.to_json
  # end

  # Parses headers as array of pairs key, value.
  #
  # @param headers [Hash].
  # @return [Array<Hash>].
  def parse_headers(headers)
    headers.map { |header| { "#{header['key']}" => header['value'] } }.inject(:merge)
  end

  # Returns array of pair [:title, :id] for select input.
  #
  # @return [Array<Array<String>>].
  def k6_test_types
    K6TestType.sorted.map { |type| [type.title, type.id.to_s] }
  end

  # Returns array of pair [:title, :id] for select input.
  #
  # @return [Array<Array<String>>].
  def k6_test_scenarios
    K6TestScenario.sorted.map { |scenario| [scenario.title, scenario.id.to_s] }
  end
end
