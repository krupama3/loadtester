class K6ResultsController < ApplicationController

  require("chartkick")

  layout 'admin'

  DB_FOLDER = Rails.root.join('tmp', 'dump')
  DB_FILE_NAME = 'k6_db_dump.sql.gz'

  before_action :require_admin, only: [:index, :display_parsed_result, :k6_db_dump]
  before_action :read_parsed_file, only: [:display_parsed_result]
  before_action :remove_current_dump, only: [:k6_db_dump]

  def index
    @results = K6Result.all.sort_by { |result| result.timestamp.to_i }.reverse.group_by(&:parent)
    @non_grouped_results = @results.delete("")
  end

  def display_parsed_result
  end

  # Endpoint to provide current DB dump.
  def k6_db_dump
    Dir.mkdir(DB_FOLDER) unless Dir.exist?(DB_FOLDER)
    db_file_path = "#{DB_FOLDER}/#{DB_FILE_NAME}"

    db_config = ActiveRecord::Base.connection_db_config&.configuration_hash
    `mysqldump -h '#{db_config[:host]}' -u '#{db_config[:username]}' -p'#{db_config[:password]}' '#{db_config[:database]}' | gzip > #{db_file_path}`

    send_file(db_file_path, filename: DB_FILE_NAME, type: 'application/x-gzip')
  end

  # Endpoint to allow download result file
  def k6_result_download
    file_path = params[:path]
    send_file(file_path) if File.exist?(file_path)
  end

  # Endpoint, which receives test results after test finished.
  def retrieve_k6_results
    return unless valid_k6_runner_access_key?

    dir = Rails.root.join('public', 'k6_run_results')
    Dir.mkdir(dir) unless Dir.exist?(dir)
    File.open("#{dir}/#{params[:text_file_name]}", 'w') do |file|
      file.write(params[:text_file])
    end
    File.open("#{dir}/#{params[:json_file_name]}", 'w') do |file|
      file.write(params[:json_file])
    end
  end

  private

  # Validate access key
  # @return [Boolean] validation result.
  def valid_k6_runner_access_key?
    params[:access_key] == K6Settings.k6_scripts_external_runner_access_key
  end

  # Load instance of K6ParsedResult by path.
  def read_parsed_file
    @parsed_result = K6ParsedResult.new("#{params[:path]}.json")
  end

  # Removes current dump. Called before action to ensure that there will be only single dump.
  def remove_current_dump
    File.delete("#{DB_FOLDER}/#{DB_FILE_NAME}") if File.exist?("#{DB_FOLDER}/#{DB_FILE_NAME}")
  end
end
