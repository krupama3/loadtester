class K6RunnerController < ApplicationController

  # Endpoint, which receives requests to run tests from testing clients.
  def run_remote_k6_scripts
    if valid_k6_runner_access_key?
      k6_testing_client = K6TestingClient.find_by(internal_name: params[:testing_client_id])
      if k6_testing_client.present?
        script_data = params[:scripts_data].map do |script_data|
          script_data.permit(%i[script_url script_name])
        end
        K6RemoteRunnerJob.perform_later(script_data, k6_testing_client)
        render_api_ok
      else
        render_api_errors l(:k6_test_script_run_invalid_testing_client_identification, scope: :errors)
      end
    else
      render_api_errors l(:k6_test_script_run_invalid_remote_runner_access_key, scope: :errors)
    end
  end

  private

  # Validate access key
  # @return [Boolean] validation result.
  def valid_k6_runner_access_key?
    params[:access_key] == K6Settings.k6_runner_access_key
  end
end
