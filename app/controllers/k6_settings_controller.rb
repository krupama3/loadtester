class K6SettingsController < ApplicationController

  layout 'admin'
  self.main_menu = false

  before_action :require_admin
  before_action :find_k6_settings

  def edit; end

  def update
    # parse_k6_script_output_format
    if @k6_settings.update(params[:k6_settings].permit(K6Settings.permitable_attributes))
      flash[:notice] = l(:notice_successful_update)
      redirect_to k6_test_scripts_path
    else
      edit
      render action: 'edit'
    end
  end

  private

  def find_k6_settings
    @k6_settings = K6Settings.instance
  end

  # def parse_k6_script_output_format
  #   params[:k6_settings][:k6_script_output_format] = params[:k6_settings][:k6_script_output_format].join(' ')
  # end
end
