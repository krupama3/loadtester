class K6TestTypesController < ApplicationController
  layout 'admin'
  self.main_menu = false

  before_action :require_admin
  before_action :find_k6_test_type, only: [:edit, :update, :destroy]

  def index
    @k6_test_types = K6TestType.sorted.to_a
    respond_to do |format|
      format.html { render layout: false if request.xhr? }
      format.api
    end
  end

  def new
    @k6_test_type ||= K6TestType.new
    @k6_test_type.safe_attributes = params[:k6_test_type]
  end

  def create
    @k6_test_type = K6TestType.new
    @k6_test_type.safe_attributes = params[:k6_test_type]
    if @k6_test_type.save
      flash[:notice] = l(:notice_successful_create)
      redirect_to k6_test_types_path
      return
    end
    render action: 'new'
  end

  def edit
  end

  def update
    @k6_test_type.safe_attributes = params[:k6_test_type]
    if @k6_test_type.save
      respond_to do |format|
        format.html do
          flash[:notice] = l(:notice_successful_update)
          redirect_to k6_test_types_path(page: params[:page])
        end
        format.js { head 200 }
      end
    else
      respond_to do |format|
        format.html do
          edit
          render action: 'edit'
        end
        format.js { head 422 }
      end
    end
  end

  def destroy
    @k6_test_type.destroy
    redirect_to k6_test_types_path
  end

  private

  def find_k6_test_type
    @k6_test_type ||= K6TestType.find(params[:id])
  rescue ActiveRecord::RecordNotFound
    render_404
  end

end
