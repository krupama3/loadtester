class K6TestScenariosController < ApplicationController
  layout 'admin'
  self.main_menu = false

  before_action :require_admin
  before_action :find_k6_test_scenario, only: [:edit, :update, :destroy]

  def index
    @k6_test_scenarios = K6TestScenario.sorted.to_a
    respond_to do |format|
      format.html { render layout: false if request.xhr? }
      format.api
    end
  end

  def new
    @k6_test_scenario ||= K6TestScenario.new
    @k6_test_scenario.safe_attributes = params[:k6_test_scenario]
  end

  def create
    @k6_test_scenario = K6TestScenario.new
    @k6_test_scenario.safe_attributes = params[:k6_test_scenario]
    if @k6_test_scenario.save
      flash[:notice] = l(:notice_successful_create)
      redirect_to k6_test_scenarios_path
      return
    end
    render action: 'new'
  end

  def edit
    # @projects = Project.all
  end

  def update
    @k6_test_scenario.safe_attributes = params[:k6_test_scenario]
    if @k6_test_scenario.save
      respond_to do |format|
        format.html do
          flash[:notice] = l(:notice_successful_update)
          redirect_to k6_test_scenarios_path(page: params[:page])
        end
        format.js { head 200 }
      end
    else
      respond_to do |format|
        format.html do
          edit
          render action: 'edit'
        end
        format.js { head 422 }
      end
    end
  end

  def destroy
    @k6_test_scenario.destroy
    redirect_to k6_test_scenarios_path
  end

  private

  def find_k6_test_scenario
    @k6_test_scenario ||= K6TestScenario.find(params[:id])
  rescue ActiveRecord::RecordNotFound
    render_404
  end

end
