class K6TestScriptsController < ApplicationController
  require 'httparty'

  protect_from_forgery except: [:k6_test_script]

  layout 'admin'
  self.main_menu = false

  before_action :require_admin, only: %i[index new create edit update destroy]
  before_action :find_all_k6_test_scripts, only: %i[index run_all_k6_scripts]
  before_action :find_k6_test_script, only: %i[edit update destroy k6_test_script run_k6_script]
  before_action :set_base_url, only: %i[k6_test_script index run_all_k6_scripts run_k6_script]
  before_action :parse_script_configuration, only: [:k6_test_script]
  before_action :parse_scenario, only: [:k6_test_script]
  before_action :validates_k6_script_access_key, only: [:k6_test_script]

  accept_api_auth :index

  def index
    if K6Settings.visited
      respond_to do |format|
        format.html { render layout: false if request.xhr? }
        format.api
      end
    else
      redirect_to edit_k6_settings_path
    end
  end

  def new
    @k6_test_script ||= K6TestScript.new
    @k6_test_script.safe_attributes = params[:k6_test_script]
  end

  def create
    @k6_test_script = K6TestScript.new
    @k6_test_script.safe_attributes = params[:k6_test_script]
    if @k6_test_script.save
      flash[:notice] = l(:notice_successful_create)
      redirect_to k6_test_scripts_path
      return
    end
    render action: 'new'
  end

  def edit; end

  def update
    @k6_test_script.safe_attributes = params[:k6_test_script]
    if @k6_test_script.save
      respond_to do |format|
        format.html do
          flash[:notice] = l(:notice_successful_update)
          redirect_to k6_test_scripts_path(page: params[:page])
        end
        format.js { head 200 }
      end
    else
      respond_to do |format|
        format.html do
          edit
          render action: 'edit'
        end
        format.js { head 422 }
      end
    end
  end

  def destroy
    @k6_test_script.destroy
    redirect_to k6_test_scripts_path
  end

  def k6_test_script
    respond_to do |format|
      format.js
    end
  end

  # Endpoint to run all scripts in order.
  def run_all_k6_scripts
    scripts_data = prepare_scripts_data
    if K6Settings.k6_external_runner
      if K6Settings.k6_scripts_external_runner_url.present? && K6Settings.k6_scripts_external_runner_access_key.present?
        data = { scripts_data: scripts_data }.merge(prepare_params_for_remote_runner)
        post_request_for_external_runner(data)
      else
        flash[:error] = l(:k6_test_script_run_invalid_remote_runner_configuration, scope: :errors)
      end
    else
      K6LocalRunnerJob.perform_later(scripts_data)
      time = Time.now
      flash[:notice] = l(:label_k6_test_scripts_run_successful, time: time)
    end
    redirect_to k6_test_scripts_path(page: params[:page])
  end

  # Endpoint to run single script
  def run_k6_script
    script_url = resolve_k6_test_script_url(@k6_test_script)
    if script_url.present?
      script_data = [{
        script_url: script_url,
        script_name: @k6_test_script.title.parameterize.underscore
      }]
      if K6Settings.k6_external_runner
        if K6Settings.k6_scripts_external_runner_url.present? && K6Settings.k6_scripts_external_runner_access_key.present?
          data = { scripts_data: script_data }.merge(prepare_params_for_remote_runner)
          post_request_for_external_runner(data)
        else
          flash[:error] = l(:error_k6_test_script_run_invalid_remote_runner_configuration)
        end
      else
        time = Time.now
        K6LocalRunnerJob.perform_later(script_data)
        flash[:notice] = l(:label_k6_test_script_run_successful, name: @k6_test_script.title, time: time)
      end
    else
      flash[:error] = l(:k6_test_script_run_no_url, scope: :errors)
    end
    redirect_to k6_test_scripts_path(page: params[:page])
  end

  private

  def find_all_k6_test_scripts
    @k6_test_scripts = K6TestScript.sorted.to_a
  end

  def find_k6_test_script
    @k6_test_script ||= K6TestScript.find(params[:id])
  rescue ActiveRecord::RecordNotFound
    render_404
  end

  # Gets script configuration (test criteria, thresholds, duration), that defines test type.
  def parse_script_configuration
    @script_config = JSON.parse(@k6_test_script.k6_test_type.k6_script_configuration)
  end

  # Sets url that will be called during test scenario.
  # Allows to write scenario for different url then current application.
  def set_base_url
    @base_url = K6Settings.k6_scripts_base_url.presence || request.base_url
  end

  # Sets api key, that is required to get access to application through API.
  def api_key
    return @api_key if @api_key.present?

    @api_key = K6Settings.k6_scripts_api_key.present? ? K6Settings.k6_scripts_api_key : User.where(admin: true).first&.api_key
  end

  # Gets script scenario configuration. (Scenario steps)
  def parse_scenario
    @scenario_config = JSON.parse(@k6_test_script.k6_test_scenario.k6_script_configuration)
    prepare_additional_variables
    prepare_default_api_headers
    @scenario = @scenario_config['scenario']
  end

  # Extracts variables from scenario config to instance variable
  def prepare_additional_variables
    @variables = @scenario_config['variables'].presence || []
  end

  # Sets default api headers for gain access through api_key
  def prepare_default_api_headers
    @default_api_headers = { 'X-Redmine-API-Key' => api_key }
  end

  # Send request to run test externally to defined runner
  #
  # @param data [Hash] data to send to runner.
  def post_request_for_external_runner(data)
    response = HTTParty.post(prepared_runner_url,
                             body: data.to_json,
                             headers: { 'Content-Type' => 'application/json' })
    if response.success?
      time = Time.now
      message = if @k6_test_script.present?
                  l(:label_k6_test_script_run_successful, name: @k6_test_script.title, time: time)
                else
                  l(:label_k6_test_scripts_run_successful)
                end
      flash[:notice] = message
    elsif response['errors'].present?
      flash[:error] = response['errors'].join(",\n")
    else
      flash[:error] = l(:k6_test_script_run_runner_unreachable, scope: :errors)
    end
  end

  # Validates script access key to allow only authorized entity see script.
  def validates_k6_script_access_key
    return render_403 unless params[:access_key] == K6Settings.k6_script_access_key
  end

  # Forms up runner url from settings.
  #
  # @return [String] runner url.
  def prepared_runner_url
    "#{K6Settings.k6_scripts_external_runner_url}#{k6_runner_run_remote_k6_scripts_path(format: :json)}"
  end

  # Prepares data of all scripts to be send to runner or run locally.
  def prepare_scripts_data
    return [] unless @k6_test_scripts.present?

    @k6_test_scripts.map do |script|
      {
        script_url: resolve_k6_test_script_url(script),
        script_name: script.title.parameterize.underscore
      }
    end
  end

  # Prepares side information required to authorize to remote runner.
  #
  # @return [Hash].
  def prepare_params_for_remote_runner
    {
      access_key: K6Settings.k6_scripts_external_runner_access_key,
      testing_client_id: K6Settings.k6_scripts_external_runner_testing_client_id
    }
  end

  # Replace localhost domain for Docker domain.
  # Required only for local development and docker environment.
  #
  # @param script [K6TestScript]
  # @return [String] script url.
  def resolve_k6_test_script_url(script)
    if Rails.env.development? && ENV['DOCKER_HOST_URL'].present?
      k6_test_script_k6_test_script_url(script, format: :js).gsub('localhost', ENV['DOCKER_HOST_URL'])
    else
      k6_test_script_k6_test_script_url(script, format: :js)
    end
  end
end
