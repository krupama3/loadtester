class K6TestingClientsController < ApplicationController

  layout 'admin'
  self.main_menu = false

  before_action :require_admin
  before_action :find_k6_testing_client, only: [:edit, :update, :destroy]

  def index
    @k6_testing_clients = K6TestingClient.sorted.to_a
    respond_to do |format|
      format.html { render layout: false if request.xhr? }
      format.api
    end
  end

  def new
    @k6_testing_client ||= K6TestingClient.new
    @k6_testing_client.safe_attributes = params[:k6_testing_client]
  end

  def create
    @k6_testing_client = K6TestingClient.new
    @k6_testing_client.safe_attributes = params[:k6_testing_client]
    if @k6_testing_client.save
      flash[:notice] = l(:notice_successful_create)
      redirect_to k6_testing_clients_path
      return
    end
    render action: 'new'
  end

  def edit
    # @projects = Project.all
  end

  def update
    @k6_testing_client.safe_attributes = params[:k6_testing_client]
    if @k6_testing_client.save
      respond_to do |format|
        format.html do
          flash[:notice] = l(:notice_successful_update)
          redirect_to k6_testing_clients_path(page: params[:page])
        end
        format.js { head 200 }
      end
    else
      respond_to do |format|
        format.html do
          edit
          render action: 'edit'
        end
        format.js { head 422 }
      end
    end
  end

  def destroy
    @k6_testing_client.destroy
    redirect_to k6_testing_clients_path
  end

  private

  def find_k6_testing_client
    @k6_testing_client ||= K6TestingClient.find(params[:id])
  rescue ActiveRecord::RecordNotFound
    render_404
  end
end
