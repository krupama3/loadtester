#!/bin/bash -l
set -e

# Start doing anything after mysql is ready!
while ! mysql -h "${DB_HOST}" -u "${DB_USER}" -p"${DB_PASSWORD}" "${DB_NAME}" -e "SELECT 1" >/dev/null 2>&1; do
  echo "waiting for ${DB_HOST}..."
  sleep 1
done

function start() {
  gunzip < "${RAILS_DIR}/k6_db_dump.sql.gz" | mysql -h "${DB_HOST}" -u "${DB_USER}" -p"${DB_PASSWORD}" "${DB_NAME}"
  bundle exec rake db:migrate RAILS_ENV=production
  bundle exec rake redmine:plugins:migrate RAILS_ENV=production
  bundle exec puma --dir "${RAILS_DIR}" -e "${RAILS_ENV}" -p "${RAILS_PORT}" -C "${RAILS_DIR}/config/loadtester_puma.rb"
}

case "$1" in
  "start")
    start
    ;;
  *)
    echo "Unknown action"
    exit 1
    ;;
esac
