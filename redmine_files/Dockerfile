FROM ruby:3.1.2-slim-bullseye

# Install dependencies
RUN apt-get clean all; \
    apt-get update; \
    apt-get -y --no-install-recommends install \
    tree \
    git \
    uuid-dev \
    imagemagick \
    libmagickcore-dev \
    libmagickwand-dev \
    libxapian-dev \
    libsqlite3-dev \
    zip bzip2 xz-utils \
    libxslt-dev \
    libxml2-dev \
    curl rsync wget \
    gcc make g++ \
    lsb-release gnupg2 \
    gettext-base \
    vim-tiny \
    nodejs; apt-get clean

# Install mariaDb
RUN apt-get update && apt-get -y install default-libmysqlclient-dev && apt-get -y install default-mysql-client

# Install K6 (original from page)
#RUN gpg --no-default-keyring --keyring /usr/share/keyrings/k6-archive-keyring.gpg --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys C5AD17C747E3415A3642D57D77C6C491D6AC1D69
#RUN echo "deb [signed-by=/usr/share/keyrings/k6-archive-keyring.gpg] https://dl.k6.io/deb stable main" | tee /etc/apt/sources.list.d/k6.list
#RUN apt-get update && apt-get install k6

ARG APP_DIRECTORY
ARG PORT
ENV HOME "/opt/loadtester/${APP_DIRECTORY}"
ENV RAILS_DIR="${HOME}/current" \
    RAILS_MAX_WORKERS="0" \
    RAILS_PORT=${PORT} \
    TZ="Europe/Prague"

WORKDIR ${RAILS_DIR}

COPY . ${RAILS_DIR}

# Install K6 for linux platform
RUN apt-get update && apt-get install -y gnupg2 && \
    apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys C5AD17C747E3415A3642D57D77C6C491D6AC1D69 && \
    echo "deb https://dl.k6.io/deb stable main" | tee -a /etc/apt/sources.list && \
    apt-get update && apt-get install -y k6

# Install K6 binaries for MacOS platofrm
# For MacOS there default binary k6 in bin directory of project
#COPY "./bin/k6" "/usr/local/bin"

# Finally run bundle install
RUN bundle install

EXPOSE ${RAILS_PORT}/tcp
ENTRYPOINT ["./docker-entrypoint.sh"]
CMD ["start"]
