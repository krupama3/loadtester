if Rails.env.profiling?
  require 'rack-mini-profiler'
  Rack::MiniProfiler.config.position = 'bottom-left'
  Rack::MiniProfiler.config.enable_advanced_debugging_tools = true
end

