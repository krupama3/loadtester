class CreateK6TestConfigurations < ActiveRecord::Migration[6.1]
  def up
    create_table :k6_test_configurations, force: true do |t|
      t.string :title, null: false, index: true
      t.integer :position
      t.string :type, null: false, index: true

      t.json :k6_script_configuration, null: true

      t.timestamps null: false
    end
  end

  def down
    drop_table :k6_test_configurations
  end
end
