class AddSystemScenarios < ActiveRecord::Migration[6.1]

  def up
    K6TestScenario.system_scenarios_names.each do |scenario_internal_name|
      next if K6TestScenario.exists?(internal_name: scenario_internal_name)

      scenario_path = Rails.root.join('plugins', 'load_tester', 'config', 'scenarios', 'json', 'html_scenario', "#{scenario_internal_name}.json")
      K6TestScenario.create(
        title: scenario_internal_name.titleize,
        k6_script_configuration: File.read(scenario_path),
        system_configuration: true,
        internal_name: scenario_internal_name
      )
    end
  end

  def down
    K6TestScenario.where(internal_name: K6TestScenario.system_scenarios_names).destroy_all
  end

end
