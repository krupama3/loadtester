class AddSystemAttributesToK6TestConfigurations < ActiveRecord::Migration[6.1]

  def change
    add_column :k6_test_configurations, :system_configuration, :boolean, default: false
    add_column :k6_test_configurations, :internal_name, :string
  end

end
