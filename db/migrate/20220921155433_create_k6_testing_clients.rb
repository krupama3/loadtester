class CreateK6TestingClients < ActiveRecord::Migration[6.1]
  def up
    create_table :k6_testing_clients, force: true do |t|
      t.string :name, null: false, unique: true, index: true
      t.string :internal_name, null: false, unique: true, index: true
      t.string :web_url
      t.string :script_access_key
      t.integer :position

      t.timestamps null: false
    end
  end

  def down
    drop_table :k6_testing_clients
  end
end
