class AddPrometheusAttributesToK6Settings < ActiveRecord::Migration[6.1]

  def change
    add_column :k6_settings, :k6_prometheus, :boolean, default: false
    add_column :k6_settings, :k6_prometheus_user, :string
    add_column :k6_settings, :k6_prometheus_password, :string
    add_column :k6_settings, :k6_prometheus_remote_url, :string
  end

end
