class AddTestingClientIdToK6Settings < ActiveRecord::Migration[6.1]

  def change
    add_column :k6_settings, :k6_scripts_external_runner_testing_client_id, :string
  end

end
