class CreateK6TestScripts < ActiveRecord::Migration[6.1]
  def up
    create_table :k6_test_scripts, force: true do |t|
      t.string :title, null: false, index: true
      t.integer :position

      t.belongs_to :k6_test_scenario
      t.belongs_to :k6_test_type

      t.timestamps null: false
    end
  end

  def down
    drop_table :k6_test_scripts
  end
end
