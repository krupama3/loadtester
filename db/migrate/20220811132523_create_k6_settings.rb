class CreateK6Settings < ActiveRecord::Migration[6.1]
  def up
    create_table :k6_settings, force: true do |t|
      t.integer :singleton_guard, unique: true
      t.string :k6_scripts_api_key
      t.string :k6_scripts_base_url
      t.boolean :k6_external_runner, default: false
      t.string :k6_scripts_external_runner_url
      t.string :k6_scripts_external_runner_access_key
      t.string :k6_script_access_key, default: SecureRandom.base58(36)
      t.string :k6_runner_access_key, default: SecureRandom.base58(36)
      t.string :k6_script_output_format, default: ''

      t.boolean :visited, default: false

      t.timestamps null: false
    end
  end

  def down
    drop_table :k6_settings
  end
end
