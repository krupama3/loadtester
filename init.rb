Redmine::Plugin.register :load_tester do
  name 'Load Tester'
  author 'Martin Krupa'
  description 'Redmine plugin for test application loads'
  version '0.0.1'
end

Redmine::MenuManager.map :admin_menu do |menu|
  menu.push :k6_test_scripts, { controller: 'k6_test_scripts' },
            caption: :label_k6_test_script_plural,
            html: { class: 'icon icon-issue' }
end

# load all patches
Dir.glob("#{Rails.root.join('plugins', 'load_tester', 'patches')}/**/*").reject { |f| File.directory?(f) }.each { |f| require f }

# define new settings
# Setting.define_setting('k6_scripts_api_key', default: '')
# Setting.define_setting('k6_scripts_base_url', default: '')
# Setting.define_setting('k6_external_runner', default: '0')
# Setting.define_setting('k6_scripts_external_runner_url', default: '')
# Setting.define_setting('k6_scripts_external_runner_access_key', default: '')
# Setting.define_setting('k6_script_access_key', default: SecureRandom.base58(36))
# Setting.define_setting('k6_runner_access_key', default: SecureRandom.base58(36))
# Setting.define_setting('k6_scripts_output_text', default: '0')
# Setting.define_setting('k6_scripts_output_csv', default: '0')
# Setting.define_setting('k6_scripts_output_json', default: '0')

# Token instances increase for k6 testing under single user
Token.actions["session"][:max_instances] = 1000