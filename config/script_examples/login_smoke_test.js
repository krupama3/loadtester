import http from 'k6/http';
import { check, sleep, group } from 'k6';
import exec from 'k6/execution';

const time_stamp = new Date()

export const options = {
  "vus": 1,
  "duration": "1s",
  "thresholds": {
    "http_req_failed": ["rate \u003c 0.01"],
    "http_req_duration": ["p(99) \u003c 1000"]
  }
};

export default () => {

  let login_response;
  let issues_response;
  let issues_new_response;

  // console.log("----------------------------------#VU info----------------------------------------")
  //
  // console.log(`iteration number: ${exec.vu.iterationInInstance}
  // VU id: ${exec.vu.idInInstance}
  // current date: ${new Date()}`)
  // console.log("Time stamp: ${time_stamp}")

  // console.log("----------------------------------#Login----------------------------------------")
  login_response = http.get(`http://localhost:3000/login`);

  login_response = login_response.submitForm({
    formSelector: '#login-form > form',
    fields: { username: 'admin', password: 'diplomka' },
  });

  // console.log(login_response.body)

  check(login_response, {'logged in successfully': (r) => r.status === 200});

  // console.log("----------------------------------#Issues----------------------------------------")
  issues_response = http.get(`http://localhost:3000/issues`);

  // console.log(issues_response.body)

  check(issues_response, {'issues listed successfully': (r) => r.status === 200});

  // console.log("----------------------------------#Issues-new----------------------------------------")
  issues_new_response = http.get(`http://localhost:3000/issues/new`);

  issues_new_response = issues_new_response.submitForm({
    formSelector: '#issue-form',
    fields: { "issue[project_id]": 1, "issue[tracker_id]": 1, "issue[subject]": `Test k6 subject3`, "issue[status_id]": 4, "issue[priority_id]": 3 },
  });

  console.log(issues_new_response.headers)

  check(issues_new_response, {'issue created successfully': (r) => r.status === 200});

  sleep(1);
};

// Object.keys(hash).forEach(function (key) {
//   hash[key] = hash[key].replace("${uniq_stamp}", uniq_stamp);
// })