import http from 'k6/http';
import { check, sleep } from 'k6';

export function setup() {
  return {
    1: {
      username: "admin",
      password : "diplomka"
    },
    2: {
      username: "test",
      password : "diplomka"
    },
    3: {
      username: "test2",
      password : "diplomka"
    },
    4: {
      username: "test3",
      password : "diplomka"
    },
    5: {
      username: "test4",
      password : "diplomka"
    },
    6: {
      username: "test5",
      password : "diplomka"
    },
  }
}

export const options = {
  stages: [
    {duration:"2m",target:25},
    {duration:"6m",target:25},
    {duration:"2m",target:0}
  ],
  // "vus":20,
  // "duration":"1m",
  thresholds: {
    http_req_failed: ["rate\u003c0.01"],
    http_req_duration: ["p(99) \u003c 1000"]
  },
  ext: {
    loadimpact: {
      projectID: 3600694,
      name: "Test scenario"
    }
  }
};

export default (data) => {
  let response;
  let objectId;

  response = http.get(`https://diplomka-development.dev6.easysoftware.com/login`, {"tags":{"name":"login"}} );
  check(response, {'page loaded successfully': (r) => r.status === 200});

  console.log(`current user ${__VU}, username: ${data[__VU % 5 + 1]["username"]} and password: ${data[__VU % 5 + 1]["password"]}`)

  response = response.submitForm({
    formSelector: `#login-form > form`,
    fields: {"username": `${data[__VU % 5 + 1]["username"]}` ,"password": data[__VU % 5 + 1]["password"] },
  });
  check(response, {'logged in successfully': (r) => r.status === 200});

  response = http.get(`https://diplomka-development.dev6.easysoftware.com/issues`, {"tags":{"name":"issues"}} );
  check(response, {'page loaded successfully': (r) => r.status === 200});

  check(response, {'retrieved issues': (r) => r.html().find('.issue').size() > 0});

  response = http.get(`https://diplomka-development.dev6.easysoftware.com/issues/new`, {"tags":{"name":"new_issue"}} );
  check(response, {'page loaded successfully': (r) => r.status === 200});

  response = response.submitForm({
    formSelector: `#issue-form`,
    fields: {"issue[project_id]":1,"issue[tracker_id]":1,"issue[subject]":"Test k6 subject","issue[status_id]":4,"issue[priority_id]":3},
    params: {"tags":{"name":"create_issue"}},
  });
  check(response, {'issue created successfully': (r) => r.status === 200});

  response = http.get(`https://diplomka-development.dev6.easysoftware.com/issues`, {"tags":{"name":"issues"}} );
  check(response, {'page loaded successfully': (r) => r.status === 200});

  check(response, {'retrieved issues': (r) => r.html().find('.issue').size() > 0});

  objectId = response.html().find(".issue").get(0).id().replace('issue-', '')

  response = http.get(`https://diplomka-development.dev6.easysoftware.com/issues/${objectId}`, {"tags":{"name":"issue"}} );
  check(response, {'page loaded successfully': (r) => r.status === 200});

  response = http.get(`https://diplomka-development.dev6.easysoftware.com/issues/${objectId}/edit`, {"tags":{"name":"edit_issue"}} );
  response = response.submitForm({
    formSelector: `#issue-form`,
    fields: {"issue[done_ratio]":10},
    params: {"tags":{"name":"update_issue"}},
  });
  check(response, {'issue edited successfully': (r) => r.status === 200});

  response = http.get(`https://diplomka-development.dev6.easysoftware.com/projects`, {"tags":{"name":"projects"}} );
  check(response, {'page loaded successfully': (r) => r.status === 200});

  check(response, {'retrieved projects': (r) => r.html().find('.project').size() > 0});

  response = response.clickLink({
    selector: ".project:nth-child(1)",
    params: {"tags":{"name":"project"}},
  });
  check(response, {'page loaded successfully': (r) => r.status === 200});

  response = http.get(`https://diplomka-development.dev6.easysoftware.com/issues`, {"tags":{"name":"issues"}} );
  check(response, {'page loaded successfully': (r) => r.status === 200});

  check(response, {'retrieved issues': (r) => r.html().find('.issue').size() > 0});

  objectId = response.html().find(".issue").get(0).id().replace('issue-', '')


  response = http.get(`https://diplomka-development.dev6.easysoftware.com/issues/${objectId}`, {"tags":{"name":"issue"}} );
  check(response, {'page loaded successfully': (r) => r.status === 200});

  response = http.get(`https://diplomka-development.dev6.easysoftware.com/issues/${objectId}/time_entries/new`, {"tags":{"name":"new_time_entry"}} );
  check(response, {'page loaded successfully': (r) => r.status === 200});

  response = response.submitForm({
    formSelector: `#new_time_entry`,
    fields: {"time_entry[hours]":0.03,"time_entry[comments]":"My Time Entry","time_entry[user_id]":"1"},
    params: {"tags":{"name":"create_time_entry"}},
  });
  check(response, {'Time entry created successfully': (r) => r.status === 200});

  response = http.get(`https://diplomka-development.dev6.easysoftware.com/time_entries`, {"tags":{"name":"time_entries"}} );
  check(response, {'page loaded successfully': (r) => r.status === 200});

  check(response, {'retrieved time entries': (r) => r.html().find('.time-entry').size() > 0});

  objectId = response.html().find(".time-entry").get(1).id().replace('time-entry-', '')


  response = http.get(`https://diplomka-development.dev6.easysoftware.com/time_entries/${objectId}/edit`, {"tags":{"name":"edit_time_entry"}} );
  check(response, {'page loaded successfully': (r) => r.status === 200});

  response = response.submitForm({
    formSelector: `#edit_time_entry_${objectId}`,
    fields: {"time_entry[hours]":0.05},
    params: {"tags":{"name":"update_time_entry"}},
  });
  check(response, {'Time entry edited successfully': (r) => r.status === 200});

  sleep(1);
};