Rails.application.routes.draw do

  resources :k6_test_scripts, except: :show do
    collection do
      get 'run_all_k6_scripts'
    end
    member do
      get 'run_k6_script'
      get 'k6_test_script', format: :js
    end
  end
  get 'smoke_test_script', to: 'k6_test_scripts#smoke_test_script', format: :js
  get 'load_test_script', to: 'k6_test_scripts#load_test_script', format: :js
  get 'stress_test_script', to: 'k6_test_scripts#stress_test_script', format: :js
  get 'spike_test_script', to: 'k6_test_scripts#spike_test_script', format: :js
  get 'soak_test_script', to: 'k6_test_scripts#soak_test_script', format: :js

  resources :k6_test_scenarios, except: :show

  resources :k6_test_types, except: :show

  resources :k6_testing_clients, except: :show

  post 'k6_runner_run_remote_k6_scripts', to: 'k6_runner#run_remote_k6_scripts', format: :json

  get 'edit_k6_settings', to: 'k6_settings#edit'
  post 'k6_settings', to: 'k6_settings#update'

  post 'k6_results_retrieve_k6_results', to: 'k6_results#retrieve_k6_results', format: :json

  get 'k6_results', to: 'k6_results#index'
  get 'k6_results/display_parsed_result/:path', to: 'k6_results#display_parsed_result', as: 'k6_results_display_parsed_result'
  get 'k6_results/k6_db_dump', to: 'k6_results#k6_db_dump'
  get 'k6_results/k6_result_download', to: 'k6_results#k6_result_download'

end
